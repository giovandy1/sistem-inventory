<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Route::get('/produk', function () {
    return view('produk.produk');
});

Route::get('/produk', 'ProdukController@index');
Route::get('produk/create', 'ProdukController@create');
Route::post('/produk', 'ProdukController@store');
Route::get('/produk/{produkt_id}', 'ProduktController@show');
Route::get('/produk/{produk_id}/edit', 'ProdukController@edit');
Route::put('/produk/{produk_id}', 'ProdukController@update');
Route::delete('/produk/{produkt_id}', 'ProdukController@destroy');
