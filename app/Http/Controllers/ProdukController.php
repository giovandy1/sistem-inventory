<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class ProdukController extends Controller
{
    public function create()
    {
        return view('produk.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama_produk' => 'required',
            'stok' => 'required',
            'keterangan'  => 'required',
        ]);
        $query = DB::table('produk')->insert([
            "nama_produk" => $request["nama_produk"],
            "stok" => $request["stok"],
            "keterangan" => $request["keterangan"]
        ]);
        return redirect('/produk');
    }
    public function index()
    {
        $produk = DB::table('produk')->get();
        return view('produk.index', compact('produk'));
    }

    public function show($id)
    {
        $produk = DB::table('produk')->where('id', $id)->first();
        return view('produk.show', compact('produk'));
    }
    public function edit($id)
    {
        $produk = DB::table('produk')->where('id', $id)->first();
        return view('produk.edit', compact('produk'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'nama_produk' => 'required',
            'stok' => 'required',
            'keterangan'  => 'required',
        ]);

        $query = DB::table('produk')->where('id', $id)->update([
            "nama_produk" => $request["nama_produk"],
            "stok" => $request["stok"],
            "keterangan" => $request["keterangan"]
        ]);
        return redirect('/produk');
    }
    public function destroy($id)
    {
        $query = DB::table('produk')->where('id', $id)->delete();
        return redirect('/produk');
    }
}
