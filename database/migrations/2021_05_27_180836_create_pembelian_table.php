<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePembelianTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pembelian', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('suplier');
            $table->integer('harga_satuan');
            $table->integer('jumlah');
            $table->string('satuan');
            $table->integer('stok_awal');
            $table->integer('stok_akhir');
            $table->date('tanggal');
            $table->text('keterangan');
            $table->unsignedBigInteger('idproduk');
            $table->foreign('idproduk')->references('id')->on('produk');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pembelian');
    }
}
