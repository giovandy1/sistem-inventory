@extends('layout.master')

@section('title')
Produk
@endsection
@section('content')
<a href="/produk/create" class="btn btn-primary my-2">Tambah</a>
        <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Nama Produk</th>
                <th scope="col">Stok</th>
                <th scope="col">Keterangan</th>
                <th scope="col">Actions</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($produk as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->nama_produk}}</td>
                        <td>{{$value->stok}}</td>
                         <td>{{$value->keterangan}}</td>
                        <td>
                            <form action="/produk/{{$value->id}}" method="POST">
                            <a href="/produk/{{$value->id}}" class="btn btn-info">Show</a>
                            <a href="/produk/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger my-1" value="Delete">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
@endsection
