@extends('layout.master')

@section('title')
Tambah produk
@endsection
@section('content')

<div>
    <h2>Tambah produk</h2>
        <form action="/produk" method="POST">
            @csrf
            <div class="form-group">
                <label for="nama">Nama Produk</label>
                <input type="text" class="form-control" name="nama_produk" id="title" placeholder="Masukkan Title">
                @error('nama_produk')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
             <div class="form-group">
                <label for="nama">STOCK</label>
                <input type="number" class="form-control" name="stok" id="title" placeholder="Masukkan Title">
                @error('stok')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
             <div class="form-group">
                <label for="nama">Bio</label>
                <input type="text" class="form-control" name="keterangan" id="title" placeholder="Masukkan Title">
                @error('keterangan')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
</div>
@endsection
