@extends('layout.master')

@section('title')
Edit produk
@endsection
@section('content')

<div>
    <h2>Edit produk</h2>
        <form action="/produk/{{$produk->id}}" method="POST">
            @csrf
            @method('put')
            <div class="form-group">
                <label for="nama">Nama</label>
                <input type="text" class="form-control" value="{{ $produk->nama_produk }}" name="nama_produk" id="title" placeholder="Masukkan Title">
                @error('nama_produk')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
             <div class="form-group">
                <label for="nama">Stock</label>
                <input type="number" class="form-control" value="{{ $produk->stok }}" name="stok" id="title" placeholder="Masukkan Title">
                @error('stok')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
             <div class="form-group">
                <label for="nama">Bio</label>
                <input type="text" class="form-control" value="{{ $produk->keterangan }}" name="keterangan" id="title" placeholder="Masukkan Title">
                @error('keterangan')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Edit</button>
        </form>
</div>
@endsection
