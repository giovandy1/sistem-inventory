@extends('layout.master')

@section('title')
Daftar Obat-obatan
@endsection
@section('content')
<table class="table table-bordered">
  <thead>
    <tr>
      <th style="width: 10px">#</th>
      <th>Nama</th>
      <th>Deskripsi</th>
      <th>Stock</th>
      <th>Harga</th>
      <th>Aksi</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>1.</td>
      <td>Vitamin C</td>
      <td>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Quis ullam facere libero molestiae asperiores, nulla impedit commodi incidunt cum temporibus. Saepe architecto non hic odit ipsum ducimus asperiores provident est.
      </td>
      <td>60 pack</td>
       <td>Rp.150.000</td>
       <td>
           <a class="btn btn-primary btn-sm" href="#">
                              <i class="fas fa-folder">
                              </i>
                              View
                          </a>
                          <a class="btn btn-info btn-sm" href="#">
                              <i class="fas fa-pencil-alt">
                              </i>
                              Edit
                          </a>
                          <a class="btn btn-danger btn-sm" href="#">
                              <i class="fas fa-trash">
                              </i>
                              Delete
                          </a>
       </td>
    </tr>
   <tr>
      <td>1.</td>
      <td>Vitamin C</td>
      <td>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Quis ullam facere libero molestiae asperiores, nulla impedit commodi incidunt cum temporibus. Saepe architecto non hic odit ipsum ducimus asperiores provident est.
      </td>
      <td>60 pack</td>
       <td>Rp.150.000</td>
       <td>
           <a class="btn btn-primary btn-sm" href="#">
                              <i class="fas fa-folder">
                              </i>
                              View
                          </a>
                          <a class="btn btn-info btn-sm" href="#">
                              <i class="fas fa-pencil-alt">
                              </i>
                              Edit
                          </a>
                          <a class="btn btn-danger btn-sm" href="#">
                              <i class="fas fa-trash">
                              </i>
                              Delete
                          </a>
       </td>
    </tr>
  </tbody>
</table>
@endsection
